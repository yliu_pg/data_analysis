"""
A collection of image manipulation utils

Yu Liu
"""

import numpy as np
import imageio
import os
import sys
import glob
import shutil



def flatfield_correction(folder=' ', flatfield=' ', darkfield=' ', filter = '*FAM*', outdir='./corrected_images'):
    """
    Process the images in the folder using _flatfield_correction
    :param folder: raw image folder
           flatfield: filename for the flatfield reference in folder
           darkfield: filename for background in folder
    :return: None. Save corrected images in outdir along with unprocessed images
    """
    # read images
    # folder =  r'C:\Users\PG_silver\Documents\work\data\20180809\flatfield_test\A0_row1'
    print str(folder)
    os.chdir(folder)
    files = glob.glob1(folder, filter)
    raw_stack = [imageio.imread(f) for f in files]
    num_files = np.size(raw_stack, 0)

    # read flatfiled and darkfield references
    flatfield_file = glob.glob1(folder, flatfield or ' ')
    if len(flatfield_file) == 1:
        flatfield_reference = np.tile(imageio.imread(flatfield_file[0]), [num_files, 1, 1])
    else:
        flatfield_reference = np.ones_like(raw_stack)
        print("warning: no flatfield reference found. no correction attempted.")

    darkfield_file = glob.glob1(folder, darkfield or ' ')
    if len(darkfield_file) == 1:
        darkfield_reference = np.tile(imageio.imread(darkfield_file[0]), [num_files, 1, 1])
    else:
        darkfield_reference = np.zeros_like(raw_stack)
        print("warning: no darkfield reference found. no correction attempted.")

    # process and save corrected images
    corrected_stack = _flatfield_correction(raw_stack, flatfield_reference, darkfield_reference)
    corrected_folder = outdir
    if os.path.exists(corrected_folder):
        shutil.rmtree(corrected_folder)
    os.mkdir(corrected_folder)
    for (i, f) in enumerate(files):
        imageio.imwrite(corrected_folder + '\\' + f, corrected_stack[i, :, :])
    # copy other files
    all_files = glob.glob1(folder, 'row*')
    for f in all_files:
        if not os.path.exists(outdir + '\\' + f):
            shutil.copyfile(f, outdir + '\\' + f)


def _flatfield_correction(input, v, b = None):
    """
    calculate the flatfield corrected image.
    :param input: input image array or stack
    :param v: illumination reference array or stack.
    :param b: background reference array or stack.
    :return: corrected_image = (input - b)/(v - b) * mean(1/(v-b))
    """
    if b is None:
        b = np.zeros_like(input)

    # check if the dimensions are the same
    if (np.shape(input)!=np.shape(v)) or (np.shape(input)!=np.shape(b)):
        sys.exit("ERROR: flatfield correction and darkfield should be of the same shape as raw image stacks.")

    correction_factor = 1.0/np.subtract(v, b, dtype = float)
    correction_factor_mean = np.mean(correction_factor)
    corrected_image = np.multiply(np.subtract(input, b), correction_factor, dtype=float) / correction_factor_mean
    return corrected_image.astype(np.uint16)


def test_flatfield_correction():
    expected = np.array([[4520.83775024411, 3982.31296746779, 3480.21336574169, 3908.35588859911],
                         [4038.90881559349, 3566.69464061625, 3518.29824992766, 3503.26943136117]])
    expected = expected.astype(np.uint16)
    raw = np.array([[4400, 4048, 3536, 4000],
                    [3920, 3648, 3552, 3552]])
    v = np.array([[11155., 11646., 11634., 11725.],
                  [11119., 11713., 11488., 11610.]])
    b = np.array([[16., 16., 16, 16.],
                 [16., 16., 48., 16.]])
    result = _flatfield_correction(raw, v, b)
    np.testing.assert_allclose(result, expected)
    return

if __name__ == '__main__':
    # tests
    test_flatfield_correction()

    # flatfield modification of images in FAM channel
    flatfield = r'flatfield.tiff'
    darkfield = r'darkfield.tiff'
    filter = '*FAM*'
    #folder = r'C:\Users\PG_silver\Documents\work\data\20180928\20180928_1435_PGchip_Smpl7_C03G03-row01col07\RawData_referenceimage\A01_row0'
    #flatfield_correction(folder, flatfield=flatfield, darkfield=darkfield, filter=filter)

    # multiple folders
    folders = [r'C:\Users\PG_silver\Documents\work\data\20181001\20180926_1550_PGchip_Smpl7_COCDR29_BRDG5_BRTC\RawData\A01_row0_insituflatfield_true'
              ]
    for f in folders:
        flatfield_correction(f, flatfield=flatfield, darkfield=darkfield, filter=filter)
