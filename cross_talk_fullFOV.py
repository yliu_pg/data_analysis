"""
Analyze yedi_data from dye droplets to get cross-talk calibration matrix, this script is working with only FAM channel and a single FOV
"""
import os
import subprocess
import sys
import time
import json
import imageio
import glob
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import SmoothBivariateSpline
from scipy.stats import rankdata
from mpldatacursor import datacursor
import pandas as pd
from sklearn import mixture
from sklearn.cluster import KMeans
import fnmatch
from sklearn.neighbors import NearestNeighbors
from scipy.stats import gaussian_kde
import addcopyfighandler
import Tkinter as tk
import tkFileDialog
import tkMessageBox
import scipy
import time
import dcor

channels = ['FAM', 'HEX', 'ROX', 'Cy5']
concentrations = []  # nM
yedi_files = []
concentrations.append([0., 0., 0., 0.])  # nM, this has to be the first row
concentrations.append([75., 0., 0., 0.])
#concentrations.append([0., 75., 0., 0.])
#concentrations.append([0., 0., 150., 0.])
#concentrations.append([0., 0., 0., 100.])

# get calibration yedi_files
for con in concentrations:
    message_text = [ch + " = " + str(s) + "nM" for (ch, s) in zip(channels, con)]
    result = tkFileDialog.askopenfilename(title=message_text)
    folder = result.replace(result.split('/')[-1], '')
    os.chdir(folder)
    yedi_files.append(result)

# read calibration data
dye_data = [] #0: Buffer, 1: FAM dye, 2: HEX dye, 3: ROX dye, 4: Cy5 dye
for (i, con) in enumerate(concentrations):
    yedi_df = pd.DataFrame()
    with open(yedi_files[i]) as json_file:
        yedi = json.load(json_file)
    yedi_df['FAM_amplitude'] = yedi['YediData']['Amplitudes'][0]
    yedi_df['HEX_amplitude'] = yedi['YediData']['Amplitudes'][1]
    yedi_df['ROX_amplitude'] = 0
    yedi_df['Cy5_amplitude'] = 0
    yedi_df['Area'] = yedi['YediData']['Area']
    yedi_df.loc[:, 'FAM_amplitude_sum'] = yedi_df['FAM_amplitude'] * yedi_df['Area']
    yedi_df.loc[:, 'HEX_amplitude_sum'] = yedi_df['HEX_amplitude'] * yedi_df['Area']
    yedi_df.loc[:, 'ROX_amplitude_sum'] = yedi_df['ROX_amplitude'] * yedi_df['Area']
    yedi_df.loc[:, 'Cy5_amplitude_sum'] = yedi_df['Cy5_amplitude'] * yedi_df['Area']
    yedi_df['X'] = yedi['YediData']['YediLocation'][0]
    yedi_df['Y'] = yedi['YediData']['YediLocation'][1]
    yedi_df['Quality'] = yedi['YediData']['Qualities']
    yedi_df['FOV'] = yedi['YediData']['ImageID']  # from left to right, from top to bottom
    dye_data.append(yedi_df.copy())

# get assay yedi file
assay = tkFileDialog.askopenfilename(title="open assay yedi file")
folder = assay.replace(assay.split('/')[-1], '')
os.chdir(folder)
with open(assay) as json_file:
    yedi = json.load(json_file)
assay_yedi_df = pd.DataFrame()
assay_yedi_df['FAM_amplitude'] = yedi['YediData']['Amplitudes'][0]
assay_yedi_df['HEX_amplitude'] = 0
assay_yedi_df['ROX_amplitude'] = 0
assay_yedi_df['Cy5_amplitude'] = 0
assay_yedi_df['Area'] = yedi['YediData']['Area']
assay_yedi_df.loc[:, 'FAM_amplitude_sum'] = assay_yedi_df['FAM_amplitude'] * assay_yedi_df['Area']
assay_yedi_df.loc[:, 'HEX_amplitude_sum'] = assay_yedi_df['HEX_amplitude'] * assay_yedi_df['Area']
assay_yedi_df.loc[:, 'ROX_amplitude_sum'] = assay_yedi_df['ROX_amplitude'] * assay_yedi_df['Area']
assay_yedi_df.loc[:, 'Cy5_amplitude_sum'] = assay_yedi_df['Cy5_amplitude'] * assay_yedi_df['Area']

assay_yedi_df['X_abs'] = yedi['YediData']['YediLocation'][0]
assay_yedi_df['Y_abs'] = yedi['YediData']['YediLocation'][1]
assay_yedi_df['Quality'] = yedi['YediData']['Qualities']
#assay_yedi_df['FOV'] = yedi['YediData']['ImageID']
#fov_offsets_x = yedi['ImageInfo']['AlignOffsetX']
#fov_offsets_y = yedi['ImageInfo']['AlignOffsetY']
# calculate local coordinates for each droplets
# X = X_abs - fov_offset_x
# Y = Y_abs - fov_offset_y
#assay_yedi_df.loc[:, 'X'] = assay_yedi_df.apply(lambda row: row.X_abs - fov_offsets_x[int(row.FOV)], axis=1)
#assay_yedi_df.loc[:, 'Y'] = assay_yedi_df.apply(lambda row: row.Y_abs - fov_offsets_y[int(row.FOV)], axis=1)
assay_yedi_df.loc[:, 'X'] = assay_yedi_df.X_abs
assay_yedi_df.loc[:, 'Y'] = assay_yedi_df.Y_abs


# ROI
roi_size = 50.0  # use the dye droplets within 50 - 100 pixels from the assay droplet of interest
q_min = 0.9  # minimum of droplet quality
FOV_range = range(3, 14) + range(18, 29)

#assay_yedi_df = assay_yedi_df[(assay_yedi_df.FOV.isin(FOV_range)) & (assay_yedi_df.Quality > q_min)].copy()
assay_yedi_df = assay_yedi_df[assay_yedi_df.Quality > q_min].copy()


# iterate through all assay droplets
yedi_count = float(assay_yedi_df.shape[0])
counter = 0
for row in assay_yedi_df.itertuples():
    counter += 1
    if counter % 1000 == 0:
        print("{0} out of {1} processed".format(counter, yedi_count))
    # find neighbors in dye data for each concentration
    background = np.zeros(4)
    k = np.zeros([4, 4])
    k[0, 0] = 1
    k[1, 1] = 1
    k[2, 2] = 1
    k[3, 3] = 1

    for (i, con) in enumerate(concentrations):

        yedi_df = dye_data[i]

        yedi_df = yedi_df[(yedi_df.X > row.X - roi_size) & (yedi_df.X < row.X + roi_size) &
                          (yedi_df.Y > row.Y - roi_size) & (yedi_df.Y < row.Y + roi_size) &
                          (yedi_df.Quality > q_min)].copy()

        if max(con) == 0:
            background[0] = yedi_df['FAM_amplitude'].median()
            #background[1] = yedi_df['HEX_amplitude'].median()
            #background[2] = yedi_df['ROX_amplitude'].median()
            #background[3] = yedi_df['Cy5_amplitude'].median()
        else:
            fluorophore = con.index(max(con))  # index of the fluorophore that is used in the dye droplet
            if fluorophore != 3:
                k[0][fluorophore] = (yedi_df['FAM_amplitude'] - background[0]).median() / max(con)
                #k[1][fluorophore] = (yedi_df['HEX_amplitude'] - background[1]).median() / max(con)
                #k[2][fluorophore] = (yedi_df['ROX_amplitude'] - background[2]).median() / max(con)
            else:
                #k[3][fluorophore] = (yedi_df['Cy5_amplitude'] - background[3]).median() / max(con)
                pass
    # get calibration matrix
    A = np.linalg.inv(k)  # the inverse of k


    # transform and save the data
    raw_data = np.array([row.FAM_amplitude, row.HEX_amplitude, row.ROX_amplitude, row.Cy5_amplitude])
    background_subtracted = raw_data - background
    cross_talk_corrected = np.transpose(np.dot(A, np.transpose(background_subtracted)))
    assay_yedi_df.loc[row.Index, 'FAM_amplitude_c'] = cross_talk_corrected[0]
    assay_yedi_df.loc[row.Index, 'HEX_amplitude_c'] = cross_talk_corrected[1]
    assay_yedi_df.loc[row.Index, 'ROX_amplitude_c'] = cross_talk_corrected[2]
    assay_yedi_df.loc[row.Index, 'Cy5_amplitude_c'] = cross_talk_corrected[3]
