#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
#==============================================================================
#title           :RunAnalysis.py
#description     :Set up arguments and call application for data analysis
#author          :Sam YF. Ling
#CopyRight       :PreciGenome LLC
#Date created       :20171215
#Date last modified :20171215
#version         :
#notes           :
#python_version  :2.7
#==============================================================================
'''
import os
import subprocess
import sys
import time
import json
import imageio
import glob
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import SmoothBivariateSpline
from scipy.stats import rankdata
from mpldatacursor import datacursor
import pandas as pd
from sklearn import mixture
from sklearn.cluster import KMeans
import fnmatch
from sklearn.neighbors import NearestNeighbors
import addcopyfighandler
import Tkinter as tk
import tkFileDialog
import tkMessageBox
import scipy
import RunAnalysisApp
import ipywidgets as widgets

class yedi_process(object):
    """
    class to process yedi data from
    """

    def __init__(self, yedi_file=None, message_text="select yedi file"):
        self.pixel_size = 3.225  # 1 pixel in brightfield = 3.225 um
        if yedi_file == None:
            # get yedi_file from user in UI
            result = tkFileDialog.askopenfilename(title=message_text)
            folder = result.replace(result.split('/')[-1], '')
            os.chdir(folder)
            self.yedi_file = result
        else:
            self.yedi_file = yedi_file

        self.calibration = dict()
        self.yedi_df = pd.DataFrame() # data frame for yedi data
        # load information from yedi that has to be there in all cases
        self._load_common_data()
        return

    def _load_common_data(self):
        """
        load information from yedi that has to be there in all cases
        :return:
        """
        print("load " + self.yedi_file + "...")
        with open(self.yedi_file) as json_file:
            yedi = json.load(json_file)
        self.yedi_df['Area'] = yedi['YediData']['Area']
        self.yedi_df['edge_diameter'] = np.sqrt(4 * self.yedi_df.Area / np.pi) * self.pixel_size
        # get channels
        self.channel_map = {int(k.split()[1]):v for (k, v) in yedi['YediDetectionParams']['Channel Map'].iteritems()}
        for (channel_id, v) in self.channel_map.iteritems():
            if len(yedi['YediData']['Amplitudes'][channel_id])>0:
                self.yedi_df[v] = yedi['YediData']['Amplitudes'][channel_id]
                self.yedi_df[v+'_sum'] = self.yedi_df[v]*self.yedi_df['Area']
                print(v + " channel is available")
            else:
                self.yedi_df[v] = None
                self.yedi_df[v + '_sum'] = None
                print(v + " channel doesn't have data although in channel map.")

        # get droplet location
        self.yedi_df['X_abs'] = yedi['YediData']['YediLocation'][0]
        #self.yedi_df['X'] = self.yedi_df['X_abs'] # for compatibility with old cold
        self.yedi_df['Y_abs'] = yedi['YediData']['YediLocation'][1]
        #self.yedi_df['Y'] = self.yedi_df['Y_abs'] # for compatibility with old cold

        # get quality
        self.yedi_df['Quality'] = yedi['YediData']['Qualities']
        # get fov id
        self.yedi_df['FOV'] = yedi['YediData']['ImageID']  # from left to right, from top to bottom

        # get droplet location in the original FOV
        fov_offsets_x = np.array(yedi['ImageInfo']['AlignOffsetX']).flatten()
        fov_offsets_y = np.array(yedi['ImageInfo']['AlignOffsetY']).flatten()
        self.yedi_df['X_local'] = self.yedi_df.apply(lambda row: row.X_abs - fov_offsets_x[int(row.FOV)], axis=1)
        self.yedi_df['Y_local'] = self.yedi_df.apply(lambda row: row.Y_abs - fov_offsets_y[int(row.FOV)], axis=1)

        # get image file names
        self.images = [i for l in yedi['ImageInfo']['ImageNameIndex'] for i in l]
        new_path="/".join(self.yedi_file.split('/')[:-3])
        self.images =[new_path+"/"+"/".join(f.split('/')[2:]) for f in self.images]

    def showFOV(self, fov):
        """
        show a particular FOV, double clicking on the center of the droplet shows its signal
        :return:
        """
        def onClick(event):
            if event.dblclick:
                x = np.int(np.round(event.xdata))
                y = np.int(np.round(event.ydata))
                selected_yedi = self.yedi_df[(self.yedi_df.FOV == fov) &
                                             (((np.abs(self.yedi_df.X_local-x)<20)&(np.abs(self.yedi_df.Y_local-y)<20))|
                                              ((np.abs(self.yedi_df.X_local-x-12000)<20)&(np.abs(self.yedi_df.Y_local-y-1400)<20)))]
                print(selected_yedi[[c for (i, c) in self.channel_map.iteritems()]])
                #plt.scatter(0, signal[i], marker='x', label="droplet #" + str(i))
                #plt.scatter(range(1, neighbor_count + 1), signal[neighbor_index[i, 1:]], marker='o', label="neighbors")
                #plt.legend()
                #plt.ylabel(column)
            return
        fig, ax = plt.subplots(1, len(self.channel_map)+1)
        bf_image_id = fov%16+(int)(fov/16)*(len(self.channel_map)+1)*16
        fig.suptitle(self.images[bf_image_id])
        # show bright field
        brightfield = imageio.imread(self.images[bf_image_id])
        ax[0].imshow(brightfield)
        ax[0].set_title('bright_field')
        for (channel_id, v) in self.channel_map.iteritems():
            fl_image = imageio.imread(self.images[bf_image_id+(channel_id+1)*16])
            ax[channel_id+1].imshow(fl_image)
            ax[channel_id+1].set_title(v)
        cid = fig.canvas.mpl_connect('button_press_event', onClick)



    def exportDroplets(self, fov, export_channel='FAM'):
        """
        show a particular FOV, double clicking on the center of the droplet will export its image
        :return:
        """
        def onClick(event):
            if event.dblclick:
                x = np.int(np.round(event.xdata))
                y = np.int(np.round(event.ydata))
                selected_yedi = self.yedi_df[(self.yedi_df.FOV == fov) &
                                             (((np.abs(self.yedi_df.X_local-x)<20)&(np.abs(self.yedi_df.Y_local-y)<20))|
                                             ((np.abs(self.yedi_df.X_local-x-12000)<20)&(np.abs(self.yedi_df.Y_local-y-1400)<20)))]
                fov_img = fl_image[export_channel]
                for row in selected_yedi.itertuples():
                    yedi_box_size = row.edge_diameter/self.pixel_size*1.1
                    yedi_center_x = row.X_local if row.X_local < 12000 else row.X_local-12000
                    yedi_center_y = row.Y_local if row.Y_local < 1400 else row.Y_local-1400
                    yedi_filename='d_'+str(fov)+'_'+str(row.Index)+'.tif'
                    imageio.imsave(yedi_filename, fov_img[int(yedi_center_y-yedi_box_size/2):int(yedi_center_y+yedi_box_size/2),
                                   int(yedi_center_x-yedi_box_size/2):int(yedi_center_x+yedi_box_size/2)])
            return
        fig, ax = plt.subplots(1, len(self.channel_map)+1)
        bf_image_id = fov%16+(int)(fov/16)*(len(self.channel_map)+1)*16
        fig.suptitle(self.images[bf_image_id])
        # show bright field
        brightfield = imageio.imread(self.images[bf_image_id])
        ax[0].imshow(brightfield)
        ax[0].set_title('bright_field')
        fl_image = dict()
        for (channel_id, v) in self.channel_map.iteritems():
            fl_image[v] = imageio.imread(self.images[bf_image_id+(channel_id+1)*16])
            ax[channel_id+1].imshow(fl_image[v])
            ax[channel_id+1].set_title(v)
        cid = fig.canvas.mpl_connect('button_press_event', onClick)



    def plot2d(self, title='title ='):
        """
        generate 2d plot with user selected values for x and y from self.yedi_df
        :return:
        """
        print("plot droplet data ...")
        RunAnalysisApp.plot2d(self.yedi_df, title)

    def get_NNdiameter(self, isplot=False):
        """
        calculate diameter of each droplet based on nearest neighbor distance.
        :return:
        """
        print("calculate nearest neighbor diameter ...")
        coords = np.array(self.yedi_df[['X_abs', 'Y_abs']])
        nbrs = NearestNeighbors(n_neighbors=2, algorithm='ball_tree').fit(coords)
        distances, _ = nbrs.kneighbors(coords)
        self.yedi_df.loc[:, 'NNdiameter'] = distances[:, 1] * self.pixel_size
        distances_filtered = distances[np.argwhere(distances[:, 1] < 50), 1]
        bins = np.linspace(20, 50, 100)
        distribution, edge = np.histogram(distances_filtered, bins)
        self.NNdiameter = self.pixel_size * edge[np.argmax(distribution)]
        if isplot:
            fig, ax = plt.subplots(1, 1)
            plt.hist(distances_filtered, bins=bins, density=True)
            plt.title('D_NN mode = {} um'.format(self.NNdiameter))


    def do_filter_(self):
        """
        method to filter out droplets based on simple metrics:
        edge diameter multiple by 1.115
        size:
        quality metric:
        Amplitude:
        Location:
        :return:
        """
        print("filter droplets ...")
        # sizing
        self.yedi_df.edge_diameter = self.yedi_df.edge_diameter * 1.115
        mean_edge = self.yedi_df.edge_diameter.mean()
        std_edge = self.yedi_df.edge_diameter.std()
        edge_diameter_ub = mean_edge + 3 * std_edge
        edge_diameter_lb = mean_edge - 3 * std_edge
        edge_diameter_ub = 102
        sizing_condition = ((self.yedi_df.edge_diameter > edge_diameter_lb) &
                            (self.yedi_df.edge_diameter < edge_diameter_ub))
        # quality
        quality_lb = 0.85
        quality_condition = (self.yedi_df.Quality > quality_lb)

        # amplitude
        amplitude_ub = 60000
        amplitude_conditions = (np.all([self.yedi_df[c] < amplitude_ub for c in self.channel_map.values()], 0))

        # location
        x_range = [[2500, 12000],[13500, 25000]]
        location_conditions = (((self.yedi_df.X_abs > x_range[0][0]) & (self.yedi_df.X_abs < x_range[0][1])) |
                               ((self.yedi_df.X_abs > x_range[1][0]) & (self.yedi_df.X_abs < x_range[1][1])))

        filter_conditions = np.all([sizing_condition, quality_condition, amplitude_conditions, location_conditions], 0)
        self.yedi_df = self.yedi_df[filter_conditions].copy()



    def do_neighbor_normalization(self, neighbor=20, norm_method=RunAnalysisApp.normfactor_from_GMM3):
        """
        perform normalization using neighbors
        :return:
        """
        for c in self.channel_map.values():
            print("normalized by neighbors, channel --" + c + "-- ...")
            RunAnalysisApp.normalize_by_neighbor_wrapper(neighbor, self.yedi_df, c, method=norm_method)


    def do_clustering(self, columns=None):
        """
        do clustering results and plots
        :return:
        """
        if columns == None:
            all_columns = self.yedi_df.keys()
            for (i, k) in enumerate(all_columns):
                print("{} ==> ".format(i) + k)
            selections = input("select which columns to cluster, comma separated, end with comma.")
        for s in selections:
            RunAnalysisApp.get_PG_cluster(self.yedi_df, all_columns[s], 2, 1)

    def do_calibration_singleFOV(self, calibration_file=None, channels=["FAM"], roi_size=150):
        """
        calibration yedi data using calibration file from a single FOV. simple normalization by neighbors
        :return:
        """
        # read calibration file
        for c in channels:
            self.calibration[c] = yedi_process(message_text="load calibration json file for " + c)

        # iterate over all droplets
        yedi_count = float(self.yedi_df.shape[0])
        counter = 0
        for row in self.yedi_df.itertuples():
            counter += 1
            if counter % 1000 == 0:
                print("{0} out of {1} processed".format(counter, yedi_count))
            # find neighbors in dye data for each channel and record normfactor
            for c in channels:
                calib_yedi = self.calibration[c].yedi_df
                calib_yedi_local = calib_yedi[(calib_yedi.X_local > row.X_local - roi_size) &
                                              (calib_yedi.X_local < row.X_local + roi_size) &
                                              (calib_yedi.Y_local > row.Y_local - roi_size) &
                                              (calib_yedi.Y_local < row.Y_local + roi_size)
                                              ]
                if len(calib_yedi_local) > 0:
                    self.yedi_df.loc[row.Index, c+'_normfactor'] = calib_yedi_local[c].median()
                else:
                    self.yedi_df.loc[row.Index, c+'_normfactor'] = calib_yedi[c].median()

            #normalize
            for c in channels:
                self.yedi_df.loc[row.Index, c+'_norm'] = self.yedi_df.loc[row.Index, c]/self.yedi_df.loc[row.Index, c+'_normfactor']

    def do_matrix_calibration(self, calibration_file=None, roi_size=150):
        """
        perform color calibration and signal normalization by using calibration files
        :param calibration_file:
        :return:
        """
        # set up calibration matrix mask based on user input and color map
        for (c, name) in self.channel_map.iteritems():
            print("{} => {}".format(c, name))
        cross_talk_channels = input("select which channels have cross talk. comma separated. enter -1 to ignore. must end with comma.")
        # setup the mask for calibration matrix
        self.calibration_mask = np.eye(len(self.channel_map))
        self.calibration_matrix_local = np.eye(len(self.channel_map))
        if min(cross_talk_channels) >= 0:
            rows, cols = np.meshgrid(cross_talk_channels, cross_talk_channels)
            for (r, c) in zip(rows.flatten(), cols.flatten()):
                self.calibration_mask[r, c] = 1

        # read calibration files
        for (c, c_name) in self.channel_map.iteritems():
            self.calibration[c_name] = yedi_process(message_text="load calibration json file for " + c_name)

        # iterate over all droplets and perform signal conversion
        yedi_count = float(self.yedi_df.shape[0])
        counter = 0
        for row in self.yedi_df.itertuples():
            counter += 1
            if counter % 1000 == 0:
                print("{0} out of {1} processed".format(counter, yedi_count))
            # find neighbors in dye data for each channel and record normfactor
            raw_data = np.ones(len(self.channel_map))
            for (c, c_name) in self.channel_map.iteritems():
                raw_data[c] = getattr(row, c_name)
                calib_yedi = self.calibration[c_name].yedi_df
                calib_yedi_local = calib_yedi[(calib_yedi.X_local > row.X_local - roi_size) &
                                              (calib_yedi.X_local < row.X_local + roi_size) &
                                              (calib_yedi.Y_local > row.Y_local - roi_size) &
                                              (calib_yedi.Y_local < row.Y_local + roi_size)
                                              ]
                for (c2, c2_name) in self.channel_map.iteritems():
                    if len(calib_yedi_local) > 0:
                        self.calibration_matrix_local[c2][c] = calib_yedi_local[c2_name].median()
                    else:
                        self.calibration_matrix_local[c2][c] = calib_yedi[c2_name].median()
                self.calibration_matrix_local = np.multiply(self.calibration_matrix_local, self.calibration_mask)

            # normalize
            A = np.linalg.inv(self.calibration_matrix_local)
            cross_talk_corrected = np.transpose(np.dot(A, np.transpose(raw_data)))
            for (c, c_name) in self.channel_map.iteritems():
                self.yedi_df.loc[row.Index, c_name+'_c'] = cross_talk_corrected[c]



if __name__== '__main__':
    a = yedi_process()
    a.get_NNdiameter(True)
    a.do_filter_()
    a.do_calibration_singleFOV()
    a.do_matrix_calibration()
    a.do_neighbor_normalization()
    a.do_clustering()