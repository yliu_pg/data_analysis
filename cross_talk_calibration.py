"""
Analyze yedi_data from dye droplets to get cross-talk calibration matrix
"""
import os
import subprocess
import sys
import time
import json
import imageio
import glob
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import SmoothBivariateSpline
from scipy.stats import rankdata
from mpldatacursor import datacursor
import pandas as pd
from sklearn import mixture
from sklearn.cluster import KMeans
import fnmatch
from sklearn.neighbors import NearestNeighbors
from scipy.stats import gaussian_kde
import addcopyfighandler
import Tkinter as tk
import tkFileDialog
import tkMessageBox
import scipy


channels = ['FAM', 'HEX', 'ROX', 'Cy5']
concentrations = [] # nM
yedi_files = []
concentrations.append([0., 0., 0., 0.]) # nM, this has to be the first row
concentrations.append([150., 0., 0., 0.])
concentrations.append([0., 75., 0., 0.])
concentrations.append([0., 0., 150., 0.])
#concentrations.append(([0., 0., 0., 100.]))

# get yedi_files
for con in concentrations:
    message_text = [ch+" = "+str(s) + "nM" for (ch, s) in zip(channels, con)]
    result = tkFileDialog.askopenfilename(title = message_text )
    folder = result.replace(result.split('/')[-1], '')
    os.chdir(folder)
    yedi_files.append(result)

# get assay yedi file
assay = tkFileDialog.askopenfilename(title = "open assay yedi file")
folder = assay.replace(assay.split('/')[-1], '')
os.chdir(folder)
with open(assay) as json_file:
    yedi = json.load(json_file)
assay_yedi_df = pd.DataFrame()
assay_yedi_df['FAM_amplitude'] = yedi['YediData']['Amplitudes'][0]
assay_yedi_df['HEX_amplitude'] = yedi['YediData']['Amplitudes'][1]
assay_yedi_df['ROX_amplitude'] = yedi['YediData']['Amplitudes'][2]
assay_yedi_df['Cy5_amplitude'] = yedi['YediData']['Amplitudes'][3]
assay_yedi_df['X'] = yedi['YediData']['YediLocation'][0]
assay_yedi_df['Y'] = yedi['YediData']['YediLocation'][1]
assay_yedi_df['Quality'] = yedi['YediData']['Qualities']
assay_yedi_df['FOV'] = yedi['YediData']['ImageID']

# ROI
x_step = 150
x_min_all = range(18000, 22000, x_step)
y_step = 150
y_min_all = range(1400, 2600, y_step)
grid_x, grid_y = np.meshgrid(x_min_all, y_min_all)
q_min = 0.9

for (x_min, y_min) in zip(grid_x.flatten(), grid_y.flatten()):
    # get background and transfer matrix without spatial resolution
    background = []
    k = np.zeros([4, 4])
    k[0, 0] = 1
    k[1, 1] = 1
    k[2, 2] = 1
    k[3, 3] = 1
    for (i, con) in enumerate(concentrations):
        yedi_df = pd.DataFrame()
        with open(yedi_files[i]) as json_file:
            yedi = json.load(json_file)
        yedi_df['FAM_amplitude'] = yedi['YediData']['Amplitudes'][0]
        yedi_df['HEX_amplitude'] = yedi['YediData']['Amplitudes'][1]
        yedi_df['ROX_amplitude'] = yedi['YediData']['Amplitudes'][2]
        yedi_df['Cy5_amplitude'] = yedi['YediData']['Amplitudes'][3]
        yedi_df['X'] = yedi['YediData']['YediLocation'][0]
        yedi_df['Y'] = yedi['YediData']['YediLocation'][1]
        yedi_df['Quality'] = yedi['YediData']['Qualities']
        yedi_df['FOV'] = yedi['YediData']['ImageID']  # from left to right, from top to bottom

        yedi_df = yedi_df[(yedi_df.X > x_min) & (yedi_df.X < x_min + x_step) &
                          (yedi_df.Y > y_min) & (yedi_df.Y < y_min + y_step) &
                          (yedi_df.Quality > q_min)].copy()
        if max(con) == 0:
            background.append(yedi_df['FAM_amplitude'].median())
            background.append(yedi_df['HEX_amplitude'].median())
            background.append(yedi_df['ROX_amplitude'].median())
            background.append(yedi_df['Cy5_amplitude'].median())
        else:
            fluorophore = con.index(max(con)) # index of the fluorophore that is used in the dye droplet
            k[0][fluorophore] = (yedi_df['FAM_amplitude']-background[0]).median()/max(con)
            k[1][fluorophore] = (yedi_df['HEX_amplitude']-background[1]).median()/max(con)
            k[2][fluorophore] = (yedi_df['ROX_amplitude']-background[2]).median()/max(con)
            k[3][fluorophore] = (yedi_df['Cy5_amplitude']-background[3]).median()/max(con)
    A = np.linalg.inv(k) # the inverse of k

    # process ROI from a real 4-plex data set
    # from left to right, from top to bottom
    assay_yedi_roi = assay_yedi_df[(assay_yedi_df.X > x_min) & (assay_yedi_df.X < x_min + x_step) &
                                   (assay_yedi_df.Y > y_min) & (assay_yedi_df.Y < y_min + y_step) &
                                   (assay_yedi_df.Quality > q_min)].copy()

    # cross-talk correction
    raw_data = np.array(assay_yedi_roi[['FAM_amplitude', 'HEX_amplitude', 'ROX_amplitude', 'Cy5_amplitude']])
    background_subtracted = raw_data - background
    cross_talk_corrected = np.transpose(np.dot(A, np.transpose(background_subtracted)))
    assay_yedi_df.loc[(assay_yedi_df.X > x_min) & (assay_yedi_df.X < x_min + x_step) &
                                   (assay_yedi_df.Y > y_min) & (assay_yedi_df.Y < y_min + y_step) &
                                   (assay_yedi_df.Quality > q_min), 'FAM_amplitude_c'] = cross_talk_corrected[:, 0]
    assay_yedi_df.loc[(assay_yedi_df.X > x_min) & (assay_yedi_df.X < x_min + x_step) &
                                   (assay_yedi_df.Y > y_min) & (assay_yedi_df.Y < y_min + y_step) &
                                   (assay_yedi_df.Quality > q_min), 'HEX_amplitude_c'] = cross_talk_corrected[:, 1]
    assay_yedi_df.loc[(assay_yedi_df.X > x_min) & (assay_yedi_df.X < x_min + x_step) &
                                   (assay_yedi_df.Y > y_min) & (assay_yedi_df.Y < y_min + y_step) &
                                   (assay_yedi_df.Quality > q_min), 'ROX_amplitude_c'] = cross_talk_corrected[:, 2]
    assay_yedi_df.loc[(assay_yedi_df.X > x_min) & (assay_yedi_df.X < x_min + x_step) &
                                   (assay_yedi_df.Y > y_min) & (assay_yedi_df.Y < y_min + y_step) &
                                   (assay_yedi_df.Quality > q_min), 'Cy5_amplitude_c'] = cross_talk_corrected[:, 3]


# get background and transfer matrix with spatial resolution
all_yedi_df = pd.DataFrame()
for (i, con) in enumerate(concentrations):
    yedi_df = pd.DataFrame()
    with open(yedi_files[i]) as json_file:
        yedi = json.load(json_file)
    yedi_df['FAM_amplitude'] = yedi['YediData']['Amplitudes'][0]
    yedi_df['HEX_amplitude'] = yedi['YediData']['Amplitudes'][1]
    yedi_df['ROX_amplitude'] = yedi['YediData']['Amplitudes'][2]
    yedi_df['Cy5_amplitude'] = yedi['YediData']['Amplitudes'][3]
    yedi_df['X'] = yedi['YediData']['YediLocation'][0]
    yedi_df['Y'] = yedi['YediData']['YediLocation'][1]
    yedi_df['Quality'] = yedi['YediData']['Qualities']
    yedi_df['FOV'] = yedi['YediData']['ImageID']  # from left to right, from top to bottom
    if max(con) == 0:
        yedi_df.loc[:, 'FAM_background'] = yedi_df.FAM_amplitude
        yedi_df.loc[:, 'HEX_background'] = yedi_df.HEX_amplitude
        yedi_df.loc[:, 'ROX_background'] = yedi_df.ROX_amplitude
        yedi_df.loc[:, 'Cy5_background'] = yedi_df.Cy5_amplitude
    else:
        fluorophore = con.index(max(con)) # index of the fluorophore that is used in the dye droplet
        yedi_df.loc[:, 'k'+'0'+str(fluorophore)] = yedi_df['FAM_amplitude']/max(con)
        yedi_df.loc[:, 'k'+'1'+str(fluorophore)] = yedi_df['HEX_amplitude']/max(con)
        yedi_df.loc[:, 'k'+'2'+str(fluorophore)] = yedi_df['ROX_amplitude']/max(con)
        yedi_df.loc[:, 'k'+'3'+str(fluorophore)] = yedi_df['Cy5_amplitude']/max(con)
    all_yedi_df = all_yedi_df.append(yedi_df, ignore_index=True)

yedi_files.append(r'C:\Users\PG_silver\Documents\work\data\20181129\dye_calibration\20181129_1439_PGchip_Smpl7_BR_dropdet_V0_11')