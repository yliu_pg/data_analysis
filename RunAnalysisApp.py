#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
#==============================================================================
#title           :RunAnalysis.py
#description     :Set up arguments and call application for data analysis
#author          :Sam YF. Ling
#CopyRight       :PreciGenome LLC
#Date created       :20171215
#Date last modified :20171215
#version         :
#notes           :
#python_version  :2.7
#==============================================================================
'''
import os
import subprocess
import sys
import time
import json
import imageio
import glob
import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import SmoothBivariateSpline
from scipy.stats import rankdata
from mpldatacursor import datacursor
import pandas as pd
from sklearn import mixture
from sklearn.cluster import KMeans
import fnmatch
from sklearn.neighbors import NearestNeighbors
from scipy.stats import gaussian_kde
import addcopyfighandler
import Tkinter as tk
import tkFileDialog
import tkMessageBox
import scipy
#from Src.DataReadWrite.CombineYediData import CombineYediData

class RunAnalysis(object):

    def __init__(self, pathAnalysisApp):
        self.pathAnalysisApp = pathAnalysisApp
        return

    def GetAnalysisAppPath(self):
        return self.pathAnalysisApp

    def AnalyzeSingleArea(self):
        """
        Analyze single area of images
        """
        return

    def RunApp(self, listImageFiles=[]):
        print '\n Call ibdropimage_test...'
        pathApp = r'D:\GitDev\ImageProcess\opencv\winbuild\libdropimage\x64\Release\libdropimage_test.exe '
        dirData = r' D:\GitDev\SoftwareDesign\pyControl\OutputDataFiles\112\RawData'
        #dirOutPut = r' D:\GitDev\SoftwareDesign\pyControl\OutputDataFiles\112'
        dirOutPut = os.path.dirname(dirData)
        optInBright = r' -input_file_bright '
        optInFluor_ch1 = r' -input_file_fluoren_ch0 '
        optInFluor_ch2 = r' -input_file_fluoren_ch1 '

        optOutContourImg = r' -output_file_img '
        optOutJson = r' -output_file_json'
        args =  pathApp + \
                optInBright + dirData + r'\row01_col01_ch0_brightfield_period_2secs.tiff' + \
                optInFluor_ch1 + dirData + r'\row01_col01_ch3_TexasRed_period_2secs.tiff'+ \
                optInFluor_ch2 + dirData + r'\row01_col01_ch4_Cy5_period_2secs.tiff'+ \
                optOutContourImg + dirOutPut + r'\python_test_output_contour.png'+ \
                optOutJson + dirOutPut + r'\well_E1_TRed200nm_Cy5_200_25nm_1len.yedi.json'+   \
                r' -preproc'+ r' median_threshold' + \
                r' -output_image_type' + r' bright'


        #subprocess.call(args)
        subprocess.Popen(args)  # non-blocking
        print 'arges:', args
        return

    def DisplayLibVersionInfor(self):
        pathApp = self.GetAnalysisAppPath()
        opt = r' -version'
        args = pathApp + \
               opt
        subprocess.Popen(args)  # non-blocking
        return

    def AnalizebyRow(self, dirRowImages, strRowName='', dirRawData='', strVersion=''):
        """
        Stitch images in a row, and analyzed stitched images
        """
        print '\n Call ibdropimage_test...'
        #pathApp = r'D:\GitDev\ImageProcess\opencv\winbuild\libdropimage\x64\Release\libdropimage_test.exe '
        #dirData = r'.\RawData\A_row02'
        pathApp = self.GetAnalysisAppPath()


        #f = os.listdir(dirData)
        #dirOutput = os.path.dirname( dirData )
        dirOutput = os.path.join(os.path.dirname(os.path.dirname(dirRawData)), "YediData_%s\RowsData"%(strVersion))
        if not os.path.exists(dirOutput):
            os.makedirs(dirOutput)

        optStitch = r' -stitch_mode yes '
        optImageFolder = r' -input_folder '


        optOutContourImg = r' -output_file_img '
        optOutJson = r' -output_file_json '
        strContourFileName = r'\stitched_contour_%s.png'%(strRowName)

        strJsonFileName = r'\well_E01_%s.yedi.json'%(strRowName)   ##output yedi.json file
        pathJsonYediFile = dirOutput + strJsonFileName # os.path.join(dirOutput, strJsonFileName)

        #################################
        args =  pathApp + \
                optStitch + \
                optImageFolder + dirRawData + \
                optOutContourImg + dirOutput + strContourFileName + \
                optOutJson + pathJsonYediFile +   \
                r' -preproc'+ r' median_threshold' + \
                r' -output_image_type' + r' bright' + \
                r' -resolution 1_4M'

        subprocess.call(args)    # blocking call
        #subprocess.Popen(args)  # non-blocking
        print 'arges:', args
        return pathJsonYediFile

    def AnalizeSingleArea(self, dirData, fileImages, strLoc=r'',  optRes = r' -resolution 1_4M'):
        """
        Analyze images for single area
        :param dirData:
        :param fileImages:
        :param strLoc:
        :return:
        """
        print '\n Call ibdropimage_test...'
        pathApp = self.GetAnalysisAppPath()
        dirOutput = os.path.dirname( dirData )

        optInBright = r' -input_file_bright '
        optInFluor_ch0 = r' -input_file_fluoren_ch0 '
        optInFluor_ch1 = r' -input_file_fluoren_ch1 '

        optOutContourImg = r' -output_file_img '
        optOutJson = r' -output_file_json '
        nameContour = r'\%s_output_contour.png'%(strLoc)
        nameJsonFile = r'\%s_well_A01_TRed200nm_Cy5_200_25nm_1len.yedi.json'%(strLoc)
        args =  pathApp + \
                r' -stitch_mode no' + \
                optInBright + fileImages[0] + \
                optInFluor_ch0 + fileImages[1] + \
                optInFluor_ch1 + fileImages[2] + \
                optOutContourImg + dirOutput + nameContour + \
                optOutJson + dirOutput + nameJsonFile +   \
                r' -preproc'+ r' median_threshold' + \
                r' -output_image_type' + r' bright' + \
                optRes
        #
        # dirData + "\\" +
        subprocess.call(args)
        # subprocess.Popen(args)  # non-blocking
        print 'arges:\n', args
        return


def get_yedi_data(data_folder, stitch_mode="one_chamber",
                  pathApp=r'C:\DropsProcess\libdropimage_test.exe'):
    """
    Method to perform stitching and image analysis
    :param stitch_mode: one_chamber, two_chamber or single_area
    :param data_folder: folder name for images
    :param pathApp: location of libdropimage_test.ext
    :return: full path to yedi.json file
    """
    tBegin = time.time()
    print 'Test script for RunAnalsis..-----------------------------'
    instRunAnalysis = RunAnalysis(pathApp)
    # instRunAnalysis.RunApp()

    instRunAnalysis.DisplayLibVersionInfor()
    available_modes = dict({'single_area': 0,
                            'one_chamber': 1,
                            'two_chamber': 2})
    if not stitch_mode in available_modes.keys():
        sys.exit("Please choose single_area, one_chamber or two_chamber")
    else:
        stitch_mode = available_modes[stitch_mode]

    if (stitch_mode == 2):
        print "\n 2 chambers stitch_mode testing .. please wait....\n----------------------------------"

        # master folder of the row raw data
        dirDataRows = data_folder

        ## get plate name

        strPlateName = os.path.basename(os.path.dirname(dirDataRows))
        listRowDir = []
        for dir in os.listdir(dirDataRows):
            if r'_row' in dir:
                print "Row raw folder", dir
                listRowDir.append(os.path.join(dirDataRows, dir))

        listYediFiles = []
        ir = 0
        for dirRow in listRowDir:
            dirOutput = instRunAnalysis.AnalizebyRow(None, strRowName="row3_1_4M_python_row%d" % (ir + 1),
                                                     dirRawData=dirRow)
            ir += 1
            listYediFiles.append(dirOutput)

        instCombine = CombineYediData()
        pathCombined = os.path.join(dirDataRows, r'Combined_%s.yedi.json' % (strPlateName))
        print "listYediFiles:  \n", listYediFiles
        ret = instCombine.Combine(listYediFiles, pathCombined)
        print "-------------------\nCombined file:", pathCombined

    elif (stitch_mode == 1):
        print "\nstitch_mode testing .. please wait....\n----------------------------------"
        '''    
        ## Stitching test.
        dirData = r'D:\Optics\Experiement_Data\Suzhou_Data\20171225_dropsize_test26_3\RawData\A_row02'
        dirOutput = instRunAnalysis.AnalizebyRow(None, strRowName="Row02_python_ttt", dirRawData=dirData)        
        '''

        ## Stitching test.
        # dirData = r'D:\GitDev\SoftwareDesign\GeneCount\pyControl\OutputDataFiles\20180724_0944_PGchip00_11\RawData\A0_row2'
        dirData = data_folder
        dirOutput = instRunAnalysis.AnalizebyRow(None, strRowName="row2_1_4M_python_ttt", dirRawData=dirData)

    else:  ## single-area testing

        '''        
        dirData = r'D:\GitDev\SoftwareDesign\GeneCount\pyControl\OutputDataFiles\20180717_1442_FAM100nm_10nm\RawData'
        listImages = []
        listImages.append(r'row01_col01_ch0_brightfield_ExciterRing__.tiff')
        listImages.append(r'row01_col01_ch1_FAM_ExciterBlue__.tiff')
        listImages.append(r'row01_col01_ch2_HEX_ExciterGrn__.tiff')
        '''
        dirData = data_folder
        listImages = [os.path.join(dirData, f) for f in glob.glob1(dirData, '*.tiff')]
        dirOutput = instRunAnalysis.AnalizeSingleArea(dirData, listImages, strLoc='Test')
    tEnd = time.time()
    print "\n ---------> Runtime： ", (tEnd - tBegin), 'sec'
    print "Analysis is done. Output directory:\n", dirOutput
    return dirOutput


def get_BR_cluster():
    """
    Read cluster data from BR and print s-value
    :param file: location of the BR data file
    :return: s-value, cluster_statistics
    """
    # read file
    file = tkFileDialog.askopenfilename()
    folder = file.replace(file.split('/')[-1], '')
    os.chdir(folder)
    data_df = pd.read_csv(file)
    cluster_stats = data_df.groupby('Cluster').describe()
    s_value = (cluster_stats.iloc[1]['Ch1 Amplitude']['mean']-cluster_stats.iloc[0]['Ch1 Amplitude']['mean'])/(cluster_stats.iloc[1]['Ch1 Amplitude']['std'] + cluster_stats.iloc[0]['Ch1 Amplitude']['std'])
    print "Ch1: s_value between cluster 1 and 0 is {}".format(s_value)
    s_value = (cluster_stats.iloc[1]['Ch2 Amplitude']['mean']-cluster_stats.iloc[0]['Ch2 Amplitude']['mean'])/(cluster_stats.iloc[1]['Ch2 Amplitude']['std'] + cluster_stats.iloc[0]['Ch2 Amplitude']['std'])
    print "Ch2: s_value between cluster 1 and 0 is {}".format(s_value)
    return  s_value, cluster_stats


def get_PG_cluster(yedi_data1, column, n_clusters = 2, isplot = 0):
    """
    Calculate s-value of clusters from PG reader result
    :param yedi_data1: data_frame of yedi_data
           column: name of the column to be analyzed
            n_clusters: number of cluster
            isplot: whether a plot is desired
    :return: s-value, cluster_statistics
    """
    # clustering
    yedi_data1.dropna(inplace=True)
    kmeans = KMeans(n_clusters=n_clusters, random_state=0).fit(np.array(yedi_data1[column]).reshape(-1, 1))
    cluster_header = column + '_cluster'
    if kmeans.cluster_centers_[1] > kmeans.cluster_centers_[0]:
        yedi_data1.loc[:, cluster_header] = kmeans.labels_
    else:
        yedi_data1.loc[:, cluster_header] = 1 - kmeans.labels_
    df_summary = yedi_data1.groupby(cluster_header).describe()
    p_neg = df_summary.loc[0][column]['count']/(df_summary.loc[0][column]['count'] + df_summary.loc[1][column]['count'])
    s_value = (df_summary.iloc[1][column]['mean'] - df_summary.iloc[0][column]['mean']) / (
        np.std(yedi_data1[yedi_data1[cluster_header]==0][column]) + np.std(yedi_data1[yedi_data1[cluster_header]==1][column])) # std from describe doesn't work when there is only 1 data point
    if isplot:
        fig, ax = plt.subplots(1, 2)
        ax[0].plot(yedi_data1[yedi_data1[cluster_header] == 0]['X_abs'], yedi_data1[yedi_data1[cluster_header] == 0][column], 'o', markersize=5, alpha=0.5)
        ax[0].plot(yedi_data1[yedi_data1[cluster_header] == 1]['X_abs'], yedi_data1[yedi_data1[cluster_header] == 1][column], 'x', markersize=5, alpha=0.5)
        ax[0].set_xlabel('X_abs')
        ax[0].set_ylabel(column)
        ax[1].plot(yedi_data1[yedi_data1[cluster_header] == 0]['Y_abs'], yedi_data1[yedi_data1[cluster_header] == 0][column], 'o', markersize=5, alpha=0.5)
        ax[1].plot(yedi_data1[yedi_data1[cluster_header] == 1]['Y_abs'], yedi_data1[yedi_data1[cluster_header] == 1][column], 'x', markersize=5, alpha=0.5)
        ax[1].set_xlabel('Y_abs')
        ax[1].set_ylabel(column)
        fig.text(0, 0.8, "s_value = {}, p_neg = {}, c_drop = {}".format(s_value, p_neg, -np.log(p_neg)))
    return s_value, df_summary

def plot_multiple_yedi(folder, channel = 0):
    """
    Plot multiple yedi amplitudes vs X
    :param folder: folder containing all well data
           channel: FAM, 0; HEX, 1
    :return:
    """
    channels = {0: 'FAM_amplitude', 1: 'HEX_amplitude'}
    os.chdir(folder)
    all_yedi_files = []
    for root, dirnames, filenames in os.walk('./'):
        for filename in fnmatch.filter(filenames, 'Combined*.yedi.json'):
            all_yedi_files.append(os.path.join(root, filename))
    fig, axes = plt.subplots(1, len(all_yedi_files))
    for (i, file) in enumerate(all_yedi_files):
        # read json
        with open(file) as json_file:
            yedi = json.load(json_file)
        yedi_df = pd.DataFrame()
        yedi_df['FAM_amplitude'] = yedi['YediData']['Amplitudes'][0]
        yedi_df['HEX_amplitude'] = yedi['YediData']['Amplitudes'][1]
        yedi_df['X'] = yedi['YediData']['YediLocation'][0]
        yedi_df['Y'] = yedi['YediData']['YediLocation'][1]
        yedi_df['Quality'] = yedi['YediData']['Qualities']
        yedi_filtered = yedi_df[(yedi_df['X'] > 000) & (yedi_df['X'] < 9999000)
                         & (yedi_df['FAM_amplitude'] > 00) & (yedi_df['FAM_amplitude'] < 99999900)
                         & (yedi_df['Quality']>0.85)]
        # plot
        axes[i].scatter(yedi_filtered['X'], yedi_filtered[channels[channel]], s=5, alpha=0.2)
        axes[i].set_title(file.split("\\")[-1], {'fontsize':4}, loc='right')
        if (i == 0):
            max_y = np.max(yedi_filtered[channels[channel]])*1.2
        axes[i].set_ylim(0, max_y)

def normalize_by_neighbor_wrapper(n, df, column, method = np.median):
    """
    normalize droplet signal by using the nearest N neighbors.
    :param n: number of neighbors
    :param df: dataframe containing coordinates and amplitudes
    :param column: column name to be processed
    :param method: method to calculate normalization factor from neighbor signal
    :return: nothing, df modified in place
    """
    neighbor_count = n
    for fov in df['FOV'].unique():
        df_fov = df[df['FOV'] == fov]
        result_column = column + "_" + method.__name__
        df.loc[df['FOV'] == fov, result_column] = _normalize_by_neighbor(n, df_fov, column, method)
    return

def _normalize_by_neighbor(n, df, column, method = np.median):
    """
    normalize droplet signal by using the nearest N neighbors.
    :param n: number of neighbors
    :param df: dataframe containing coordinates and amplitudes
    :param column: column name to be processed
    :param method: method to calculate normalization factor from neighbor signal
    :return: normalized signal
    """
    neighbor_count = min(n, df[column].count()-1)
    coords = np.array(df[['X', 'Y']])
    signal = np.array(df[column])
    nbrs = NearestNeighbors(n_neighbors=neighbor_count, algorithm='ball_tree').fit(coords)
    distances, neighbor_index = nbrs.kneighbors(coords)
    norm_signal = []
    for i in neighbor_index[:, 0]:
        # remove points if there exist very bright neighbors
        if np.max(signal[neighbor_index[i, 0:]])>30000:
            norm_factor = np.nan
        else:
            norm_factor = method(signal[neighbor_index[i, :]])
        norm_signal.append(1.0*signal[i]/norm_factor)
        #norm_signal.append(1.0*signal[i] - norm_factor)
    return norm_signal

def get_neighbor_signals(n, df, column, i):
    """
    get the signal of a droplet's neighbors given its index in dataframe
    :param n: number of neighbors
    :param df: dataframe containing coordinates and signals
    :param column: signal column name
    :param i: index of the droplet in interest
    :return: signal of the droplet and the neighbors
    """
    neighbor_count = min(n, df[column].count() - 1)
    coords = np.array(df[['X', 'Y']])
    signal = np.array(df[column])
    nbrs = NearestNeighbors(n_neighbors=neighbor_count + 1, algorithm='ball_tree').fit(coords)
    distances, neighbor_index = nbrs.kneighbors(coords)
    plt.figure()
    plt.scatter(0, signal[neighbor_index[i, 0]], marker='x', label="droplet #" + str(i))
    plt.scatter(range(1, n+1), signal[neighbor_index[i, 1:]], marker='o', label="neighbors")
    plt.legend()
    plt.ylabel(column)
    return signal[neighbor_index[i, :]].copy()



def normfactor_from_cluster(amplitudes):
    """
    calculate the normalization factor based on the clustering result of neighbors
    :param amplitudes: amplitudes of neighbors
    :return: the smaller of the mean of the two clusters or the median of all based on difference between the clusters
    """
    # clustering
    kmeans = KMeans(2, random_state=0).fit(np.array(amplitudes).reshape(-1, 1))
    labels = kmeans.labels_
    mean1 = np.mean(amplitudes * labels)
    mean2 = np.mean(amplitudes * (1-labels))
    std1 = np.std(amplitudes * labels)
    std2 = np.std(amplitudes * (1-labels))
    if np.abs(mean1/mean2 - 1) < 7:
        # only one cluster
        normfactor = np.median(amplitudes)
    else:
        normfactor = min(mean1, mean2)
    return normfactor

def normfactor_from_GMM(amplitudes):
    """
    calculate the normalization factor based on GMM clustering of neighbors
    :param amplitudes: amplitudes of neighbors
    :return: smaller of the means
    """
    X = amplitudes.reshape(-1, 1)
    lowest_bic = np.infty
    bic = []
    n_components_range = [1, 2]
    cv_types = ['spherical']
    for cv_type in cv_types:
        for n_components in n_components_range:
            # Fit a Gaussian mixture with EM
            gmm = mixture.GaussianMixture(n_components=n_components,
                                          covariance_type=cv_type)
            gmm.fit(X)
            bic.append(gmm.bic(X))
            if bic[-1] < lowest_bic:
                lowest_bic = bic[-1]
                best_gmm = gmm
    y = best_gmm.predict(X) # 1 for background cluster, 0 for signal cluster
    #return np.sum(np.multiply(labels, X))/np.sum(labels)
    # return np.nanmin([np.median(X[np.argwhere(y == 1)]), np.median(X[np.argwhere(y == 0)])])
    return np.nanmin([np.mean(X[np.argwhere(y == 1)]), np.mean(X[np.argwhere(y == 0)])])

def normfactor_from_GMM3(amplitudes):
    """
    calculate the normalization factor based on GMM clustering of neighbors
    :param amplitudes: amplitudes of neighbors
    :return: 1 cluster: median, 2 cluster: smaller of the means
    """
    X = amplitudes.reshape(-1, 1)
    lowest_bic = np.infty
    bic = []
    # try 2 components first
    n_components = 2
    cv_types = ['spherical']
    gmm = mixture.GaussianMixture(n_components=n_components, covariance_type=cv_types[0])
    gmm.fit(X)
    if (gmm.means_[0]/gmm.means_[1] > 0.5) and (gmm.means_[0]/gmm.means_[1] < 1.5):
        return np.median(amplitudes)
    else:
        y = gmm.predict(X) # 1 for background cluster, 0 for signal cluster
        return np.nanmin([np.mean(X[np.argwhere(y == 1)]), np.mean(X[np.argwhere(y == 0)])])


def normalize_by_smoothing(df):
    """
    normalize 'FAM_amplitude' by dividing signal with a smoothed curved fit from background droplet signals
    :param df: droplet data
    :return: droplet data after normalization
    """
    # get a rough clustering
    kmeans = KMeans(n_clusters=2, random_state=0).fit(np.array(df['FAM_amplitude']).reshape(-1, 1))
    df.loc[:, 'label'] = kmeans.labels_
    if df[df['label']==0]['FAM_amplitude'].mean() < df[df['label']==1]['FAM_amplitude'].mean():
        background_label = 0
    else:
        background_label = 1
    # fit a curve
    bvspline = SmoothBivariateSpline(df[df['label'] == background_label]['X'], df[df['label'] == background_label]['Y'],
                                                   df[df['label'] == background_label]['FAM_amplitude'])
    # return fitted result
    return df['FAM_amplitude']/bvspline.ev(df['X'], df['Y'])


def process_yedijson(channels, neighbor=20, norm_method=np.median):
    """
    process one or more yedi.json file and return the result as a dataframe
    :param channels: list of channels to be processed: "FAM", "HEX", "TR", "Cy5"
           neighbor: number of neighbors used to calculate the normalization factor
    :return: a dictionary. keys are filenames, values are the corresponding dataframe after processing
    """
    if_add_more = True
    result_list = list()
    while (if_add_more):
        # read file
        result = tkFileDialog.askopenfilename()
        folder = result.replace(result.split('/')[-1], '')
        os.chdir(folder)
        result_list.append(result)
        if_add_more = tkMessageBox.askyesno("", "Add more files to process?")
    # process json files
    process_results = dict()
    # get json files

    for result in result_list:
        print("Start processing " + result)
        folder = result.replace(result.split('/')[-1], '')
        os.chdir(folder)
        # folder = r'C:\Users\PG_silver\Documents\work\data\20181010\398\20181010_1536_PGchip_Smpl8_398-BHQ_dropdet_V0_11\YediData\RowsData'
        # row_id = 1 #1 or 2
        # all_jsons = glob.glob1(folder, "*.yedi.json")
        # result = all_jsons[row_id-1]
        with open(result) as json_file:
            yedi = json.load(json_file)

        # extract useful info
        yedi_df = pd.DataFrame()
        if len(yedi['YediData']['Amplitudes'][0]) > 0:
            yedi_df['FAM_amplitude'] = yedi['YediData']['Amplitudes'][0]
        if len(yedi['YediData']['Amplitudes'][1]) > 0:
            yedi_df['HEX_amplitude'] = yedi['YediData']['Amplitudes'][1]
        if len(yedi['YediData']['Amplitudes'][2]) > 0:
            yedi_df['TR_amplitude'] = yedi['YediData']['Amplitudes'][2]
        if len(yedi['YediData']['Amplitudes'][3]) > 0:
            yedi_df['Cy5_amplitude'] = yedi['YediData']['Amplitudes'][3]
        yedi_df['X'] = yedi['YediData']['YediLocation'][0]
        yedi_df['Y'] = yedi['YediData']['YediLocation'][1]
        yedi_df['Area'] = yedi['YediData']['Area']
        area_std = yedi_df.Area.std()
        area_mean = yedi_df.Area.mean()
        yedi_df['Quality'] = yedi['YediData']['Qualities']
        yedi_df['FOV'] = yedi['YediData']['ImageID']  # from left to right, from top to bottom

        # cross-talk correction, no spatial resolution. DO NOT USE.
        #background = [4708.5, 1791.0, 3230.0, 287.0]
        #A = np.array([[ 8.39982228e-03, -1.44122847e-03,  4.97017458e-04,
        # 2.28866453e-03],
        #[-1.44851196e-04,  7.59776160e-03, -4.16292928e-04,
        # 3.81325445e-04],
        #[ 9.16257016e-05, -9.04847896e-03,  1.44069550e-02,
        # 1.51501428e-03],
        #5.44740984e-06, -9.40170860e-06,  7.56394310e-06,
        # 7.31673900e-03]])
        #raw_data = np.array(yedi_df[['FAM_amplitude', 'HEX_amplitude', 'TR_amplitude', 'Cy5_amplitude']])
        #background_subtracted = raw_data - background
        #cross_talk_corrected = np.transpose(np.dot(A, np.transpose(background_subtracted)))
        #yedi_df.loc[:, ['FAM_amplitude', 'HEX_amplitude', 'TR_amplitude', 'Cy5_amplitude']] = cross_talk_corrected
        #

        for c in channels:
            yedi_df.loc[:, c + '_amplitude'] = yedi_df[c + '_amplitude'] / yedi_df[c + '_amplitude'].mean()
            yedi_df.loc[:, c + '_sum'] = yedi_df[c + '_amplitude'] * yedi_df['Area']
            yedi_df.loc[:, c + '_sum'] = yedi_df[c + '_sum'] / np.mean(yedi_df[c + '_sum'])
            yedi_df.loc[:, c + '_norm'] = yedi_df[c + '_amplitude'] / np.sqrt(yedi_df['Area'])

        # extract useful droplets
        FOV_range = range(3, 14) + range(18, 29)
        yedi_data1 = yedi_df[(yedi_df['Quality'] > 0.85) &
                             (yedi_df['Area'] > area_mean - 0.5 * area_std) & (
                                         yedi_df['Area'] < area_mean + 0.5 *area_std) &
                             (yedi_df['FOV'].isin(FOV_range))].copy()
        yedi_data1.index = range(yedi_data1.shape[0])
        # convert coordinates to one FOV
        # width = 896.625
        # yedi_data1.loc[:, 'X'] = yedi_data1.X % width
        # yedi_data1.loc[:, 'FOV'] = 0

        # further processing
        for c in channels:
            print("Start processing channel: " + c)
            get_PG_cluster(yedi_data1, c+'_amplitude', 2, 1)
            plt.gca().set_title(result.split('/')[-1] +": " + c)
            normalize_by_neighbor_wrapper(neighbor, yedi_data1, c+'_amplitude', method=norm_method)
            get_PG_cluster(yedi_data1, c+'_amplitude'+'_'+norm_method.__name__, 2, 1)
            plt.gca().set_title(result.split('/')[-1] +": " + c)
            normalize_by_neighbor_wrapper(neighbor, yedi_data1, c + '_sum', method=norm_method)
            get_PG_cluster(yedi_data1, c + '_sum' + '_' + norm_method.__name__, 2, 1)
            plt.gca().set_title(result.split('/')[-1] + ": " + c)
            # normalize_by_neighbor_wrapper(neighbor, yedi_data1, c+'_norm', method=norm_method)
            # get_PG_cluster(yedi_data1, c+'_norm'+'_'+norm_method.__name__)
            print("Done processing channel: " + c)

        process_results[result] = yedi_data1
        print("Done processing " + result)
    return process_results

def show_neighbors(df, column):
    """
    methods to show the neighbor of a droplet of interest
    :param df: dataframe after get_PG_cluster
    :param column: name of the signal column
    :return: plot 1: column vs row number, user select data points to show neighbor by zooming in and double clicking as close as possible
             plot 2: raw intensity of the droplet and its neighbors
    """
    def onClick(event):
        if event.dblclick:
            i = np.int(np.round(event.xdata))
            plt.figure()
            plt.scatter(0, signal[i], marker='x', label="droplet #" + str(i))
            plt.scatter(range(1, neighbor_count + 1), signal[neighbor_index[i, 1:]], marker='o', label="neighbors")
            plt.legend()
            plt.ylabel(column)
        return
    neighbor_count = 20
    coords = np.array(df[['X', 'Y']])
    signal = np.array(df[column])
    nbrs = NearestNeighbors(n_neighbors=neighbor_count + 1, algorithm='ball_tree').fit(coords)
    distances, neighbor_index = nbrs.kneighbors(coords)
    fig = plt.figure()
    plt.plot(signal, 'x')
    cid = fig.canvas.mpl_connect('button_press_event', onClick)
    plt.show(block=False)


def plot2d(df, title=''):
    """
    plot user selected columns of a dataframe
    :param df: inuput dataframe.
           title: title of the plot
    :return:
    """
    print("-1 ==> droplet ID")
    columns = df.keys()
    for (i, k) in enumerate(columns):
        print("{} ==> ".format(i) + k)
    x_key = input("select x value")
    if x_key == -1:
        x_data = np.arange(df.shape[0])
        x_label = 'droplet'
    else:
        x_data = df[columns[x_key]]
        x_label = columns[x_key]
    y_key = input("select y value")
    plt.figure()
    plt.plot(x_data, df[columns[y_key]], 'o', alpha=0.5)
    plt.xlabel(x_label)
    plt.ylabel(columns[y_key])
    plt.title(title)

def max_likelyhood_c(V, label, method='PG'):
    """
    Estimate concentration based on all droplet volumes and their label
    :param V: array, droplet volume in nL
    :param label: array, droplet classification. 1 is positive, 0 is negative
    :param method: PG or IVL (https://pubs.acs.org/doi/10.1021/acs.analchem.8b01988)
    :return: concentration in copies/nL
    """
    V_pos = V[np.argwhere(label == 1)].flatten()
    V_neg = V[np.argwhere(label == 0)].flatten()
    # this is the derivative of log likelyhood w.r.t c
    if method == 'PG':
        f = lambda c: np.sum(np.exp(-V_pos*c)*V_pos/(1-np.exp(-c*V_pos))) - np.sum(V_neg)
    elif method == 'IVL':
        f = lambda c: 1.0 - (1.0/len(V))*np.sum(np.exp(-c*V)) - np.mean(label)
    else:
        sys.exit("method not found")
    return scipy.optimize.brentq(f, 0.001, 10)


if __name__ == '__main__':
    # analyze images
    folder = r'C:\Users\PG_silver\Documents\work\data\20190128\inlet_BF_infocus\analysis'
    get_yedi_data(folder, stitch_mode='single_area')

    # get json file
    result = tkFileDialog.askopenfilename()
    folder = result.replace(result.split('/')[-1], '')
    os.chdir(folder)
    #folder = r'C:\Users\PG_silver\Documents\work\data\20181010\398\20181010_1536_PGchip_Smpl8_398-BHQ_dropdet_V0_11\YediData\RowsData'
    #row_id = 1 #1 or 2
    #all_jsons = glob.glob1(folder, "*.yedi.json")
    #result = all_jsons[row_id-1]
    with open(result) as json_file:
        yedi = json.load(json_file)

    # extract useful info
    yedi_df = pd.DataFrame()
    if len(yedi['YediData']['Amplitudes'][0])>0:
        yedi_df['FAM_amplitude'] = yedi['YediData']['Amplitudes'][0]
    if len(yedi['YediData']['Amplitudes'][1])>0:
        yedi_df['HEX_amplitude'] = yedi['YediData']['Amplitudes'][1]
    if len(yedi['YediData']['Amplitudes'][2])>0:
        yedi_df['TR_amplitude'] = yedi['YediData']['Amplitudes'][2]
    if len(yedi['YediData']['Amplitudes'][3])>0:
        yedi_df['Cy5_amplitude'] = yedi['YediData']['Amplitudes'][3]
    yedi_df['X'] = yedi['YediData']['YediLocation'][0]
    yedi_df['Y'] = yedi['YediData']['YediLocation'][1]
    yedi_df['Area'] = yedi['YediData']['Area']
    area_std = yedi_df.Area.std()
    area_mean = yedi_df.Area.mean()
    yedi_df['Quality'] = yedi['YediData']['Qualities']
    yedi_df['FOV'] = yedi['YediData']['ImageID'] # from left to right, from top to bottom


    # extract useful droplets
    FOV_range = range(3, 14) + range(18, 29)
    yedi_data1 = yedi_df[(yedi_df['Quality'] > 0.85) &
                         (yedi_df['Area'] > area_mean - 2*area_std) & (yedi_df['Area'] < area_mean + 2*area_std) &
                         (yedi_df['FOV'].isin(FOV_range))].copy()
    yedi_data1.index = range(yedi_data1.shape[0])

    # further processing
    neighbor = 15
    channels = ['FAM', 'HEX'] # 'FAM, HEX, 'TR', 'Cy5'
    for c in channels:
        print("Start processing channel: " + c)
        yedi_data1.loc[:, c+'_amplitude'] = yedi_data1[c+'_amplitude']/yedi_data1[c+'_amplitude'].mean()
        yedi_data1.loc[:, c+'_sum'] = yedi_data1[c+'_amplitude']*yedi_data1['Area']
        yedi_data1.loc[:, c+'_norm'] = yedi_data1[c+'_amplitude']/np.sqrt(yedi_data1['Area'])
        normalize_by_neighbor_wrapper(neighbor, yedi_data1, c+'_amplitude', method=np.median)
        normalize_by_neighbor_wrapper(neighbor, yedi_data1, c+'_sum', method=np.median)
        normalize_by_neighbor_wrapper(neighbor, yedi_data1, c+'_norm', method=np.median)
        print("Done processing channel: " + c)

    neighbor_max = 100
    s_values = []
    for (i, n) in enumerate(range(5, neighbor_max)):
        s_values.append(np.abs(get_PG_cluster(normalize_by_neighbor(n, yedi_data1, method=normfactor_from_cluster), 2)[0]))
    plt.figure()
    plt.plot(range(5, neighbor_max), s_values)
    plt.title(folder)
    plt.xlabel('neighbor count')
    plt.ylabel('s-value')






    # get NN distance after filtering out some droplets
    #yedi_data1 = yedi_df[yedi_df['Quality']>0.85]
    coords = np.array(yedi_df[['X', 'Y']])
    nbrs = NearestNeighbors(n_neighbors=2, algorithm='ball_tree').fit(coords)
    distances, _ = nbrs.kneighbors(coords)
    distances_filtered = distances[np.argwhere(distances[:, 1]<50), 1]
    bins = np.linspace(20, 50, 100)
    plt.figure()
    plt.hist(distances_filtered, bins=bins, density=True)

    #cluster_data1 = yedi_df[(yedi_df['X'] > 5000) & (yedi_df['X'] < 9000) & (yedi_df['FAM_amplitude'] < 20000) & (yedi_df['FAM_amplitude'] > 1750)]['FAM_amplitude']
    #cluster_data2 = yedi_df[(yedi_df['X'] > 17000) & (yedi_df['X'] < 25000) & (yedi_df['FAM_amplitude'] < 20000) & (yedi_df['FAM_amplitude'] > 1750)]['FAM_amplitude']
    #cluster_data = pd.concat([cluster_data1, cluster_data2])
    cluster_data = yedi_df[(yedi_df['X'] > 00) & (yedi_df['X'] < 100000) & (yedi_df['Quality'] > 0.85)]['HEX_amplitude'][0:11000]
    get_PG_cluster(cluster_data, 2)



    # plot yedi intensity and quality overlayed on stiched image
    yedi_data2 = yedi_data1[(yedi_data1['X'] > 000) & (yedi_data1['X'] < 15000)
                         & (yedi_data1['FAM_amplitude'] < 1.6) & (yedi_data1['FAM_amplitude'] > 1.3)
                         & (yedi_data1['Quality']>0.85)]
    stitched_image_file = glob.glob1("./", "*.png")
    stitched_image = imageio.imread(stitched_image_file[row_id-1])
    fig, axes = plt.subplots(1, 1)
    axes.imshow(stitched_image)
    axes.plot(yedi_data2['X']*2, yedi_data2['Y']*2, 'rx', markersize = 12)



    yedi_intensity_map = np.empty_like(stitched_image)

    x = np.array(yedi['YediData']['YediLocation'][0]) * 2
    y = np.array(yedi['YediData']['YediLocation'][1]) * 2
    yedi_quality = np.array(yedi['YediData']['Qualities'])
    yedi_quality_rank = rankdata(yedi_quality, method = 'min')
    yedi_quality_rank = 100. * yedi_quality_rank / np.max(yedi_quality_rank)
    yedi_filters = np.array(yedi['YediData']['FilteringFlags'])
    yedi_amplitudes = np.array(yedi['YediData']['Amplitudes'])
    yedi_FAM_amps_rank = rankdata(yedi_amplitudes[0])
    yedi_FAM_amps_rank = 100. * yedi_FAM_amps_rank / np.max(yedi_FAM_amps_rank)
    yedi_area = np.array(yedi['YediData']['Area'])

    fig, ax = plt.subplots(2, 2)
    ax[0][0].scatter(np.arange(yedi['YediData']['YediCount']), yedi_amplitudes[0])
    datacursor(ax[0][0])
    ax[1][0].scatter(x, yedi_amplitudes[0])
    ax[1][1].scatter(y, yedi_amplitudes[0])
    plt.show()

    plt.figure()
    plt.scatter(1.0*np.array(yedi['YediData']['YediLocation'][0]), yedi_amplitudes[0])
    """
    for (i, (ix, iy)) in  enumerate(zip(x, y)):
    yedi_intensity_map[iy-15:iy+15, ix-15:ix+15] = yedi_FAM_amps_rank[i]
    rank_map = axes[1].imshow(yedi_intensity_map)
    #datacursor(rank_map)
    plt.show()
    """

    # clustering
    get_PG_cluster(yedi_amplitudes[0], 2)


    # BR clustering
    file = tkFileDialog.askopenfilename()
    folder = file.replace(file.split('/')[-1], '')
    os.chdir(folder)
    s, clusters = get_BR_cluster(file)
    clusters['Ch1 Amplitude'][['count', 'mean', 'std']]
    clusters['Ch2 Amplitude'][['count', 'mean', 'std']]



    # Plot multiple yedi.json together
    folder = r'C:\Users\PG_silver\Documents\work\data\20180907\corrected\background_correction'
    plot_multiple_yedi(folder, 0)
