import os
import sys
import json
import glob
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import rankdata
from mpldatacursor import datacursor
import pandas as pd
import addcopyfighandler





if __name__ == '__main__':
    # read plate result file
    result = r'C:\Users\PG_silver\Documents\work\data\20180815\20180815_PCR_summary.xlsx'
    data_df = pd.read_excel(result)

    # calculate inferred results
    # condition 1: BR + BR
    selector = data_df['Condition'] == 1
    data_df.loc[selector, 'C_drop = -ln(P_neg)'] = -np.log(data_df.loc[selector, 'P_neg'])
    data_df.loc[selector, 'C_calc (copy/uL)'] = data_df.loc[selector, 'C_drop = -ln(P_neg)']/(4./3*np.pi*(.1175/2)**3)
    standard_concentration = np.mean(data_df.loc[selector, 'C_nominal (D=117.5 um) (copy/uL)'])

    # condition 2: BR + PG
    selector = data_df['Condition'] == 2
    data_df.loc[selector, 'C_drop = -ln(P_neg)'] = -np.log(data_df.loc[selector, 'P_neg'])
    data_df.loc[selector, 'C_nominal (D=117.5 um) (copy/uL)'] = data_df.loc[selector, 'C_drop = -ln(P_neg)']/(4./3*np.pi*(.1175/2)**3)
    # correction factor for D_mode based on edge
    D_mode_correction = np.mean(117.5/data_df.loc[selector, 'D_mode'])
    print "Correction factor for D_mode is {}".format(D_mode_correction)
    data_df.loc[selector, 'D_mode_corrected'] = data_df.loc[selector, 'D_mode'] * D_mode_correction
    data_df.loc[selector, 'C_calc_mode'] = data_df.loc[selector, 'C_drop = -ln(P_neg)']/(
                                                        4./3*np.pi*(data_df.loc[selector, 'D_mode_corrected']/1000/2)**3)
    # correction factor for D_neighbor based on edge
    D_neighbor_correction = np.mean(117.5/data_df.loc[selector, 'D_neighbor'])
    print "Correction factor for D_neighbor is {}".format(D_neighbor_correction)
    data_df.loc[selector, 'D_neighbor_corrected'] = data_df.loc[selector, 'D_neighbor'] * D_neighbor_correction
    data_df.loc[selector, 'C_calc_neighbor'] = data_df.loc[selector, 'C_drop = -ln(P_neg)']/(
                                                        4./3*np.pi*(data_df.loc[selector, 'D_neighbor_corrected']/1000/2)**3)

    # condition 3: PG + PG
    selector = data_df['Condition'] == 3
    data_df.loc[selector, 'C_drop = -ln(P_neg)'] = -np.log(data_df.loc[selector, 'P_neg'])
    data_df.loc[selector, 'C_nominal (D=117.5 um) (copy/uL)'] = data_df.loc[selector, 'C_drop = -ln(P_neg)']/(4./3*np.pi*(.1175/2)**3)
    # correction factor for D_mode based on edge
    data_df.loc[selector, 'D_mode_corrected'] = data_df.loc[selector, 'D_mode'] * D_mode_correction
    data_df.loc[selector, 'C_calc_mode'] = data_df.loc[selector, 'C_drop = -ln(P_neg)']/(
            4./3*np.pi*(data_df.loc[selector, 'D_mode_corrected']/1000/2)**3)
    # correction factor for D_neighbor based on edge
    data_df.loc[selector, 'D_neighbor_corrected'] = data_df.loc[selector, 'D_neighbor'] * D_neighbor_correction
    data_df.loc[selector, 'C_calc_neighbor'] = data_df.loc[selector, 'C_drop = -ln(P_neg)']/(
            4./3*np.pi*(data_df.loc[selector, 'D_neighbor_corrected']/1000/2)**3)


    # condition 4: PG + BR
    selector = data_df['Condition'] == 4
    data_df.loc[selector, 'C_drop = -ln(P_neg)'] = -np.log(data_df.loc[selector, 'P_neg'])
    data_df.loc[selector, 'C_calc (copy/uL)'] = data_df.loc[selector, 'C_drop = -ln(P_neg)']/(4./3*np.pi*(.1175/2)**3)
    # calculate diameter using the nominal concentration and C_drop
    data_df.loc[selector, 'D_calc'] = 117.5*np.power(data_df.loc[selector, 'C_calc (copy/uL)']/standard_concentration, 1/3.)

    # data_df.loc[data_df['PG*"), 'C_nominal (D=117.5 um) (copy/uL)'] = \
    #    data_df.loc[data_df['DR read'].str.contains("PG*"), 'C_drop = -ln(P_neg)']/(4/3*np.pi*(.1175/2)**3)
    # volume correction

    # show results
