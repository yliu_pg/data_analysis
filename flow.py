# -*- coding: utf-8 -*-
"""
Created on Wed Jan 25 20:53:42 2017
 
@author: yliu
"""
import logging
import numpy as np
from scipy import optimize as op

def calc_dim(low, high, args, R, what='height'):
    """ calculate one of ('height', 'width', 'length') based on given
flow resistance """
    # usage fl.calc_dim(low_guess, high_guess, {'h_um':100, 'w_um':200}, r, 'length')
    if what == 'height':
        result = op.brentq(lambda x: calc_resistance_rect(h_um=x, **args) - R, low, high)
        print ("expected = ", R)
        print (what + " = " + str(result))
        print ("check: " + str(calc_resistance_rect(h_um=result, **args)))
    if what == 'width':
        result = op.brentq(lambda x: calc_resistance_rect(w_um=x, **args) - R, low, high)
        print ("expected = ", R)
        print (what + " = " + str(result))
        print ("check: " + str(calc_resistance_rect(w_um=result, **args)))
    if what == 'length':
        result = op.brentq(lambda x: calc_resistance_rect(l_mm=x, **args) - R, low, high)
        print ("expected = ", R)
        print (what + " = " + str(result))
        print ("check: " + str(calc_resistance_rect(l_mm=result, **args)))
    # result = "'what' has to be one of ('height', 'width', 'length')"
    return result


def test_calc_dim():
    """ test the calculation of one of the three dimensions based on
desired flow resistance. """
    pass


def psi2kpa(psi):
    return psi * 6.895;

def kpa2psi(kpa):
    return kpa / 6.895;

def test_psi_kpa():
    """ test the conversion of psi and kpa"""
    expected_kpa = 1.2
    expected_psi = 0.174045
    result = np.abs(psi2kpa(expected_psi) -expected_kpa)/expected_kpa < 1e-4
    assert result, "incorrect conversion from psi to kpa"
    result = np.abs(kpa2psi(expected_kpa) -expected_psi)/expected_psi < 1e-4
    assert result, "incorrect conversion from kpa to psi"
    return

def calc_resistance_rect(h_um = 50, w_um = 150, l_mm = 10, vis_cP = 1):
    """ calculate the flow resistance of a rectangular channel. """

    # unit used
    # w_um: width in um
    # h_um: height in um
    # l_um: length in mm
    # vis_cP: viscosity in cP === mPa s
    # under this system, if the pressure drop is in Pa, the flow rate would be
    # in fL/sec. The unit for flow resistance is Pa/(fL/s)

    R = 12. * vis_cP * l_mm/(w_um * (h_um ** 3)) / ( 1 -
modifier(1.0*h_um/w_um))
    return R

def modifier(h_w = 1):
    """ calculate the series modifier in flow resistance calculation. """

    # h_w is h/w
    n = 1
    term = 1
    tol = 1e-7
    sum = 0
    while (term > tol):
        term = h_w * (192 / (n * np.pi)**5) * np.tanh(n * np.pi/(2 * h_w))
        sum = sum + term
        n = n + 2
    msg = "modifier converged after " + str(n) + " terms.\n"
    logging.info(msg)
    #print(msg)
    return sum

def test_resistance():
    """ test the calculation of flow resistance of a rectangular channel. """

    # the test case is from http://pubs.rsc.org/en/content/chapterhtml/2014/bk9781849736718-00001?isbn=978-1-84973-671-8
    # table 1.1. An estimate was provided in the table 5.1e-5.
    # DO NOT USE dolomite's calculator because they used the equation for elliptical cross-section.

    true_val = 5.0636054991028557e-05
    calc_val = calc_resistance_rect(h_um = 100, w_um = 300, l_mm = 1000)
    result = abs(true_val - calc_val)/true_val < 1e-5
    assert result, "flow resistance not correct"

def calc_velocity(Q, A):
    """ calculate the flow velocity based on flow rate and
cross-section area. """
    # Q: uL/sec
    # A: mm^2
    # v: mm/sec
    return Q/A

def test_velocity():
    """ test the calculation of flow velocity based on flow rate and
cross-section area. """

    Q = 123 # uL/sec
    A = 0.03 # mm^2
    v = Q/A # mm/sec
    result = abs(calc_velocity(Q, A) - v)/v < 1e-5
    assert result, "velocity calculation not correct."

def calc_Re(velocity, dim, vis_cP=1, density = 0.001):
    # Re = rho v d/vis
    return  (density*1e6) * (velocity*1e-3) *  (dim * 1e-6) /  (vis_cP * 1e-3)

def test_Re():
    """ test the calculation of Re number based on vis_cP, density,
velocity and critical dimension"""
    vis_cP = 1 # cP or mPa s
    density = 0.001 #g/mm^3
    velocity = 25. # mm/sec
    dim = 50 # critical dimension
    true_Re = 1.25
    result = abs(calc_Re(velocity, dim, vis_cP, density) -
true_Re)/true_Re < 1e-5
    assert result, "Re number calculation incorrect."

def test_calc_pressure():
    """ test the calculation of pressure drop based on flow rate and
flow resistance. """
    resistance = 1.5e-6 # unit is Pa/(fL/s)
    Q = 100/60.0 * 1e9 # unit is fL/s
    true_pressure = 1.5e-6 * 100/60 * 1e9 # unit is Pa
    result = (abs(calc_pressure(resistance, Q)-
true_pressure)/true_pressure < 1e-5)
    assert result, "pressure calculation incorrect."

def calc_pressure(resistance, Q):
    """ calculate pressure drop based on resistance (Pa/(fL/s)) and
flow rate (fL/s). """
    return resistance * Q

def test_calc_flow_rate():
    """ test the calculation of pressure flow rate based on pressure drop and
    flow resistance. """
    resistance = 1.5e-6  # unit is Pa/(fL/s)
    true_Q = 100 / 60.0 * 1e9  # unit is fL/s
    pressure = 1.5e-6 * 100 / 60 * 1e9  # unit is Pa
    result = (abs(calc_flow_rate(resistance, pressure)- true_Q)/true_Q < 1e-5)
    assert result, "flow rate calculation incorrect."

def calc_flow_rate(r, p):
    """ calculate flow rate based on flow resistance and pressure. """
    # r: flow resistance in Pa/(fL/s)
    # p: unit is Pa
    return p/r

def test_calc_resistance_flow():
    """ test the calculation of flow resistance based on pressure and
flow rate"""
    true_resistance = 1.5e-6 # unit is Pa/(fL/s)
    Q = 100/60.0 * 1e9 # unit is fL/s
    pressure = 1.5e-6 * 100/60 * 1e9 # unit is Pa
    result = (abs(calc_resistance_flow(pressure, Q)-
true_resistance)/true_resistance < 1e-5)
    assert result, "flow resistance calculation incorrect."

def calc_resistance_flow(pressure, Q):
    """ Calculate flow resistance based on pressure (Pa) and flow rate (fL/s)"""
    return pressure/Q

def test_calc_volume():
    """ test the calculation of channel volume. Rectangular shape. """
    h_um = 52.3
    l_mm = 23.4
    w_um = 72.8
    true_volume = 52.3/1000 * 23.4 * 72.8/1000 # unit is uL
    result = (abs(calc_volume(h_um, l_mm, w_um) -
true_volume)/true_volume < 1e-5)
    assert result, "volume calculation incorrect."

def calc_volume(h_um, l_mm, w_um):
    """ calculate channel volume in uL. """
    return h_um/1000 * l_mm * w_um/1000 * 1.0

def test_calc_resistance_round():
    """ test the calculation of flow resistance of a circular channel. """

    # the test case is from ttp://pubs.rsc.org/en/content/chapterhtml/2014/bk9781849736718-00001?isbn=978-1-84973-671-8
    # table 1.1. 2.5e-5 as listed there.
    d_um = 200 # um
    l_mm = 1000
    true_resistance = 25.465e-6
    result = (abs(calc_resistance_round(d_um, l_mm) -
true_resistance)/true_resistance < 1e-5)
    assert result, "flow resistance calculation incorrect for round channels."

def calc_resistance_round(d_um, l_mm, vis_cP = 1):
    """ calculate the flow resistance of a round channel. see
calc_resistance_rect for units."""
    return (2**7/np.pi)*vis_cP*l_mm/(d_um**4)

def test_calc_match_length():
    h0_um = 203.5;
    w0_um = 210.7;
    l0_mm = 13.4;
    h1_um = 211.5;
    w1_um = 400.3;
    l1_mm = 23.5;
    calculated_l1_0 = calc_match_length(h0_um, w0_um, l0_mm, h1_um,
w1_um, l1_mm);
    r1 = calc_resistance_rect(h_um=h0_um, w_um=w0_um, l_mm=l0_mm)
    r2 = (calc_resistance_rect(h_um=h1_um, w_um=w0_um, l_mm = calculated_l1_0) +
          calc_resistance_rect(h_um=h1_um, w_um=w1_um, l_mm = l1_mm -
calculated_l1_0))
    result = (np.abs(r1-r2)/r1) < 1e-7
    assert result, "matching length calculation not correct."


def calc_match_length(h0_um, w0_um, l0_mm, h1_um, w1_um, l1_mm):
    """ Use h0, w0, l0 as a reference, try to split l1 into two segments:
        h1, w0, l1_0 and h1, _w1_, l1_1, so that l0 and l1 have the
same flow resistance.
        return l1_1."""
    print("resistance of first channel is {}".
          format(calc_resistance_rect(h_um=h0_um, w_um=w0_um, l_mm=l0_mm)))
    l1_0 = op.brentq(lambda x: calc_resistance_rect(h_um=h0_um,
w_um=w0_um, l_mm=l0_mm) -
              calc_resistance_rect(h_um=h1_um, w_um=w0_um, l_mm=x) -
              calc_resistance_rect(h_um=h1_um, w_um=w1_um,
l_mm=l1_mm-x), 0, l1_mm);

    print("resistance of second channel is {}".
          format(calc_resistance_rect(h_um=h1_um, w_um=w0_um, l_mm=l1_0) +
              calc_resistance_rect(h_um=h1_um, w_um=w1_um, l_mm=l1_mm-l1_0)))
    print("the second channel is split into {}um-wide, {}mm-long, and {}um-width, {}mmlong".
          format(w0_um, l1_0, w1_um, l1_mm-l1_0))
    return l1_0


def main():
    """Entry point."""
    test_resistance()
    test_velocity()
    test_Re()
    test_calc_dim()
    test_calc_pressure()
    test_calc_volume()
    test_calc_resistance_round()
    test_psi_kpa()
    test_calc_flow_rate()
    test_calc_resistance_flow()
    test_calc_match_length()
    pass


if __name__ == '__main__':
    main()