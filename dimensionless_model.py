"""
A script to read experimental data on droplet size and plot against dimensional analysis models
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import addcopyfighandler


def dimensionless_param(input_df, w_c = None, Ro = None, Rw = None, Rd = None, eta=0.00077, sigma=3., h=None):
    """
    Calculate alpha, Phi, Qo, Ca and normalized volume

    :param input_df: input data
    :param eta: oil viscosity in Pa sec
    :param sigma: surface tension in N/m
    :param w_c: characteristic length label
    :param h: channel depth in m
    :param Ro: oil branch flow resistance from flow_solver. None or a label
    :param Rw: water branch flow resistance from flow solver. None or a label
    :param Rd: droplet branch flow resistance from flow solver. None or a label
    :return: input_df with the following additional columns (incomplete list)
        alpha = Pw/Po
        Phi = Qw/(2*Qo) calculated from alpha
        Oil flow rate (one branch) in nL/sec
        Ca number
        normalize volume by the cube of characteristic length
    """
    if w_c is None:
        input_df['w_c'] = 85.
    else:
        input_df['w_c'] = input_df[w_c]
    if Ro is None:
        input_df['Ro'] = 0.1553
    if Rw is None:
        input_df['Rw'] = 0.1636
    if Rd is None:
        input_df['Rd'] = 0.05918
    if h is None:
        input_df['H_um'] = 85.
    input_df['ro'] = input_df['Ro'] / input_df['Rd'] / 2.
    input_df['rw'] = input_df['Rw'] / input_df['Rd']
    input_df['alpha'] = input_df['Pw'] / input_df['Po']  # alpha = Pw/Po
    input_df['Phi'] = (input_df['alpha'] * (1 + input_df['ro']) - 1) / ((1 + input_df['rw']) - input_df['alpha'])  # Phi = Qw/(2*Qo) calculated from alpha
    input_df['Qo'] = 66.67 * (1 + (1 - input_df['alpha']) / input_df['rw']) / (1 + 1 / input_df['rw'] + 1 / input_df['ro']) * input_df['Po'] / input_df['Ro']
    # Oil flow rate (one branch) in nL/sec
    input_df['Ca'] = input_df['Qo'] * eta / (sigma * input_df['W'] * input_df['D'])  # Ca number
    input_df['V_cross'] = 0.5*(input_df['W']+input_df['D'])*input_df['O']*input_df['H_um'] # volume of the cross junction, unit pixel^2*um
    input_df['V_plug'] = 1.0 * input_df['L_plug'] * input_df['D'] * input_df['H_um'] # plug volume assuming same cross section along the length
    input_df['V_bar_sphere'] = (4. / 3) * np.pi * (input_df['Diameter'] / 2) ** 3 / input_df['V_cross']  # normalize volume by the volume of the cross junction
    input_df['V_bar_plug'] = input_df['V_plug'] /input_df['V_cross']
    return input_df


def test_dimensionless_param():
    """
    test the calculation of dimensionless params dimensionless_param()
    raw data set: C:\Users\PG_silver\Documents\work\data_analysis\fitting_exploration.ods
    """
    data_df = pd.read_excel(r'C:\Users\PG_silver\Documents\work\data_analysis\droplet_size_test.xlsx')
    data_df = dimensionless_param(data_df,
                                  w_c='H_um',
                                  eta = 0.00077, # oil viscosity in Pa sec
                                  sigma = 3., # surface tension in N/m
                                  )
    expected = data_df[['alpha_result', 'Phi_result', 'Qo_result', 'V_bar_result', 'V_cross_result']]
    calculated = data_df[['alpha', 'Phi', 'Qo', 'V_bar_sphere', 'V_cross']]
    np.testing.assert_allclose(calculated, expected, err_msg="error in dimensionless_param()!")
    return


if __name__ == '__main__':
    # run some tests if necessary
    test_dimensionless_param()
    # read existing data
    # data_df = pd.read_excel(r'C:\Users\PG_silver\Documents\work\data\20180808\20180808_integrated_chip_testing_summary.xlsx')
    data_df = pd.read_excel(r'C:\Users\PG_silver\Documents\work\analysis\dimensionless_model\droplet_size.xlsx')
    """
    calculate dimensionless parameters
    """
    data_df = dimensionless_param(data_df,
                                  Ro=True,
                                  Rw=True,
                                  Rd=True,
                                  eta=0.00077, # oil viscosity in Pa sec
                                  sigma=3., # surface tension in N/m
                                  )
    """ Plotting
    """
    fig, axes = plt.subplots(1, 1)

    # V_plug vs Pw
    for p in data_df['Po'].unique():
        axes.scatter(data_df[data_df['Po'] == p]['Pw'], data_df[data_df['Po'] == p]['V_plug'],
                        label = "Po={} psi".format(p))
    axes.set_xlabel('Pw (psi)')
    axes.set_ylabel('V__plug')

    # V_plug vs alpha, phi
    fig, axes = plt.subplots(2, len(data_df['Exp'].unique()))
    for (i, exp) in enumerate(data_df['Exp'].unique()):
        for po in data_df['Po'].unique():
            data = data_df[(data_df['Exp'] == exp) & (data_df['Po']==po)]
            # V_bar vs Phi
            axes[0, i].scatter(data['Phi'], data['V_plug'], label="Po={}psi".format(po))
            #axes[0, i].set_xscale('log')
            #axes[0, i].set_yscale('log')
            axes[0, i].set_xlabel('Phi')
            axes[0, i].set_ylabel('V_plug')
            axes[0, i].legend()
            axes[0, i].set_title(exp)
            # V_bar vs alpha
            axes[1, i].scatter(data['alpha'], data['V_plug'], label="Po={}psi".format(po))
            #axes[1, i].set_xscale('log')
            #axes[1, i].set_yscale('log')
            axes[1, i].set_xlabel('alpha')
            axes[1, i].set_ylabel('V_plug')
            axes[1, i].legend()
            axes[1, i].set_title(exp)

    # V_plug vs alpha, L_plug/L_oil
    fig, axes = plt.subplots(2, len(data_df['Exp'].unique()))
    for (i, exp) in enumerate(data_df['Exp'].unique()):
        for po in data_df['Po'].unique():
            data = data_df[(data_df['Exp'] == exp) & (data_df['Po']==po)]
            # V_bar vs Phi
            axes[0, i].scatter(data['L_plug']/data['L_oil'], data['V_plug'], label="Po={}psi".format(po))
            #axes[0, i].set_xscale('log')
            #axes[0, i].set_yscale('log')
            axes[0, i].set_xlabel('L_plug/L_oil')
            axes[0, i].set_ylabel('V_plug')
            axes[0, i].legend()
            axes[0, i].set_title(exp)
            # V_bar vs alpha
            axes[1, i].scatter(data['alpha'], data['V_plug'], label="Po={}psi".format(po))
            #axes[1, i].set_xscale('log')
            #axes[1, i].set_yscale('log')
            axes[1, i].set_xlabel('alpha')
            axes[1, i].set_ylabel('V_plug')
            axes[1, i].legend()
            axes[1, i].set_title(exp)


    # V_plug vs Phi, alpha and L_plug/L_oil
    fig, axes = plt.subplots(1, 3)
    for (i, exp) in enumerate(data_df['Exp'].unique()):
        data = data_df[data_df['Exp'] == exp]
        # V_plug vs Phi
        axes[0].scatter(data['Phi'], data['V_plug'], label=exp)
        axes[0].set_xlabel('Phi')
        axes[0].set_ylabel('V_plug')
        axes[0].legend()
        # V_plug vs alpha
        axes[1].scatter(data['alpha'], data['V_plug'], label=exp)
        axes[1].set_xlabel('alpha')
        axes[1].set_ylabel('V_plug')
        axes[1].legend()
        # V_plug vs L_plug/L_oil
        axes[2].scatter(data['L_plug']/data['L_oil'], data['V_plug'], label=exp)
        axes[2].set_xlabel('L_plug/L_oil')
        axes[2].set_ylabel('V_plug')
        axes[2].legend()



    #for a in axes[:]:
        #a.legend()

    fig, axes = plt.subplots(1, 3)
    for exp in data_df['Exp'].unique():
        data = data_df[data_df['Exp'] == exp]
        # L_plug/L_oil vs alpha
        axes[0].scatter(data['alpha'], data['L_plug']/data['L_oil'], label=exp)
        axes[0].set_xlabel('alpha')
        axes[0].set_ylabel('Lplug/Loil')
        axes[0].legend()
        # L_plug/L_oil vs phi
        axes[1].scatter(data['Phi'], data['L_plug']/data['L_oil'], label=exp)
        axes[1].set_xlabel('Phi')
        axes[1].set_ylabel('Lplug/Loil')
        axes[1].legend()
        # Phi vs alpha (as wells as theoretical range)
        axes[2].scatter(data['alpha'], data['Phi'], label=exp)
        alpha_min = 1.0/(data.iloc[0]['ro'] + 1)
        alpha_max = 1 + data.iloc[0]['rw']
        alpha_range = np.linspace(alpha_min, np.min([3, alpha_max-0.1]), 100)
        phi = (alpha_range*(1 + data.iloc[0]['ro']) - 1)/((1 + data.iloc[0]['rw']) - alpha_range)
        axes[2].plot(alpha_range, phi)
        axes[2].set_xlabel('alpha')
        axes[2].set_ylabel('Phi')
        axes[2].legend()
